class AddEmailToProfs < ActiveRecord::Migration
  def change
    add_column :profs, :email, :string
  end
end
