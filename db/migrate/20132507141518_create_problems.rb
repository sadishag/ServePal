class CreateProblems < ActiveRecord::Migration
  def change
    create_table :problems do |t|
      t.string :address
      t.string :description
      t.string :file

      t.timestamps
    end
  end
end
