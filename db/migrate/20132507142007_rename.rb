class Rename < ActiveRecord::Migration
  def up
    rename_column :offers, :prof_if, :prof_id
  end

  def down
    rename_column :offers, :prof_id, :prof_if
  end
end
