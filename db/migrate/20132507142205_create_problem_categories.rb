class CreateProblemCategories < ActiveRecord::Migration
  def change
    create_table :categories_problems do |t|
      t.integer :problem_id
      t.integer :category_id

      t.timestamps
    end
  end
end
