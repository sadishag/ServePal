class RemoveColumnProblemFromOrders < ActiveRecord::Migration
  def up
    remove_index :orders, :problem_id
    remove_column :orders, :problem_id
  end

  def down
    add_column :orders, :problem_id, :integer
    add_index :orders, :problem_id
  end
end
