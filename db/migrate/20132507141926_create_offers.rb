class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.integer :problem_id
      t.integer :prof_if
      t.integer :price
      t.datetime :arrival
      t.string :free_text

      t.timestamps
    end
  end
end
