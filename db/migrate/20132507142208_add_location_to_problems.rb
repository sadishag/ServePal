class AddLocationToProblems < ActiveRecord::Migration
  def change
    add_column :problems, :location_id, :integer
    add_index :problems, :location_id
  end
end
