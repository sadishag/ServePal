class AlterOffersArivalToString < ActiveRecord::Migration
  def up
    change_column :offers, :arrival, :string
  end

  def down
    change_column :offers, :arrival, :datetime
  end
end
