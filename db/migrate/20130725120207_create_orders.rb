class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :problem
      t.references :offer

      t.timestamps
    end
    add_index :orders, :problem_id
    add_index :orders, :offer_id
  end
end
