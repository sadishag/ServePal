# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Prof.find_or_create_by_name("John Smith").update_attribute 'email', 'masterweily-facilitator@gmail.com'
Prof.find_or_create_by_name("James Johnson").update_attribute 'email', 'masterweily-facilitator@gmail.com'
Prof.find_or_create_by_name("Robert Williams").update_attribute 'email', 'masterweily-facilitator@gmail.com'
Prof.find_or_create_by_name("Michael Jones").update_attribute 'email', 'masterweily-facilitator@gmail.com'
Prof.find_or_create_by_name("William Brown").update_attribute 'email', 'masterweily-facilitator@gmail.com'


Category.find_or_create_by_name "cleaning"
Category.find_or_create_by_name "doctor  "
Category.find_or_create_by_name "handyman"
Category.find_or_create_by_name "moving  "
Category.find_or_create_by_name "painting"
Category.find_or_create_by_name "plumbers"







