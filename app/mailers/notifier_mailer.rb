class NotifierMailer < ActionMailer::Base
  default :from => "info@servepal.com"

  # send a signup email to the user, pass in the user object that contains the user’s email address
  # ["guy@shopnfly.com","guy.gaash@gmail.com"]
  def send_profs(prof, problem, email,request)
    @prof = prof
    #"nitzanav@gmail.com"
    @problem = problem
    @request = request
      mail( :to => email,
            :from => 'info@SrvBay.com',
           :subject => "#{@prof.name}, You Have Job!")
  end

  def send_profs_new_order(prof, order, email,request)
    @prof = prof
    #"nitzanav@gmail.com"
    @order = order
    @request = request
      mail( :to => email,
            :from => 'info@SrvBay.com',
           :subject => "#{@prof.name}, You Have Order!")
  end
end
