# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/


$ ->

  ##  NEW

  $("form#new_problem").each ->
    $form = $(this)
    $form.find('input[type=submit]').css(marginLeft: -9000)
    $file_input = $form.find('#problem_file').css(marginLeft: -9000).change ->
      $form.submit()

    $('a.prof-types-list-link').each ->
      $a = $(this)
      $a.click (e) ->
        e.preventDefault()
        $file_input.click()

  ## EDIT

  $("form.edit_problem").each ->
    $form = $(this)
    $file_input = $form.find('input[type=file]').hide()

    $form.find('a.retake-film').click (e) ->
      e.preventDefault()
      $file_input.click()

    # Geolocation
    location = {}
    if navigator.geolocation
      navigator.geolocation.getCurrentPosition (position) ->
        location.longitude = position.coords.longitude
        location.latitude = position.coords.latitude
        url = "http://maps.google.com/maps/api/geocode/json?lating=#{location.latitude},#{location.longitude}&sensor=false"
        data =
          latlng: "#{location.latitude},#{location.longitude}"
          sensor: false
        $.get url,data, (response) ->
          location.json = response
          location.address = response.results[0].formatted_address
          $('form').find('#problem_address').val(location.address)






