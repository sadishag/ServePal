module PaypalHelper

  def self.create_bidding_agr email="masterweily-facilitator@gmail.com"
    request = Paypal::Express::Request.new(
        :username   => 'masterweily-servepal_api1.gmail.com',
        :password   => '1374769466',
        :signature  => 'A-6JrnOfLzLDJtj9TUX4IkRDSPvVAOIu613LNxbNfKNyTADTDsHL7rhz'

    )
    Paypal.sandbox!
    payment_request = Paypal::Payment::Request.new(
        :currency_code => "EUR", # if nil, PayPal use USD as default
        :amount        => 1,
        :description   => 1,
        :custom_fields => { :L_BILLINGTYPE0 => "MerchantInitiatedBilling",:L_PAYMENTTYPE0 => "Any" }
    )
    response = request.setup(
        payment_request,
        'localhost:3000',
        'localhost:3000'
        #complete_order_url(@order),
        #incomplete_order_url(@order),
  )


  end

  def paypal_standard_url(order,return_url,cancel_url)
    #raise shopping_create_order_paypal_notification_url(@order,@order.make_hash,@order.make_double_hash)
    values = {
        :business => order.paypal_business,
        :cmd => '_cart',
        :upload => 1,
        :return => return_url,
        :cancel => cancel_url,
        :invoice => order.id,
        #:currency_code => MyMoney.symbol_for_locale.to_s.upcase,
        :notify_url => root_url
    }
    values.merge!({
                      "amount_1" => order.price,
                      "item_name_1" => order.title,
                      "item_number_1" => order.offer_id,
                      "quantity_1" => 1 #[item.quantity.to_i,1].max # paypal: A quantity value must be an integer greater than or equal to one.
                  })
    "#{paypal_standard_base_url}#{values.to_query}"
  end

  private

  def paypal_standard_base_url
    Rails.env.production? ? "https://www.paypal.com/cgi-bin/webscr?" : "https://www.sandbox.paypal.com/cgi-bin/webscr?"
  end



end