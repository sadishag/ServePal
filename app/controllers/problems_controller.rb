class ProblemsController < ApplicationController

  before_filter do
    @prof = Prof.find_by_id(params[:prof_id]) if params[:prof_id]
  end
    ``
  # GET /problems
  # GET /problems.json
  def index
    @problems = Problem.order('id desc')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @problems }
    end
  end

  # GET /problems/1
  # GET /problems/1.json
  def show
    @problem = Problem.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @problem }
    end
  end

  # GET /problems/new
  # GET /problems/new.json
  def new
    @problem = Problem.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @problem }
    end
  end

  # GET /problems/1/edit
  def edit
    @problem = Problem.find(params[:id])
  end

  # POST /problems
  # POST /problems.json
  def create
    @problem = Problem.new(params[:problem])

    respond_to do |format|
      if @problem.save
        format.html { redirect_to edit_problem_path(@problem), notice: 'Problem was successfully updated.' }
        format.json { render json: @problem, status: :created, location: @problem }
      else
        format.html { render action: "new" }
        format.json { render json: @problem.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /problems/1
  # PUT /problems/1.json
  def update
    @problem = Problem.find(params[:id])

    respond_to do |format|
      if @problem.update_attributes(params[:problem])
        #,"guy.gaash@gmail.com"                   guy@shopnfly.com;
        #NotifierMailer.send_profs(Prof.all[2], @problem, "masterweily@gmail.com").deliver
        #NotifierMailer.send_profs(Prof.all[1], @problem, "klarinas@gmail.com").deliver
        NotifierMailer.send_profs(Prof.first, @problem, "nitzanav@gmail.com",request).deliver
        #NotifierMailer.send_profs(Prof.first, @problem, "guy.gaash@gmail.com",request).deliver
        #NotifierMailer.send_profs(Prof.first, @problem, "masterweily@gmail.com",request).deliver

        format.html { redirect_to problem_offers_path(@problem), notice: 'Problem was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @problem.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /problems/1
  # DELETE /problems/1.json
  def destroy
    @problem = Problem.find(params[:id])
    @problem.destroy

    respond_to do |format|
      format.html { redirect_to problems_url }
      format.json { head :no_content }
    end
  end
end
