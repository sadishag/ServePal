class OffersController < ApplicationController

  before_filter do
    @prof = Prof.find_by_id(params[:prof_id]) if params[:prof_id]
    @problem = Problem.find_by_id(params[:problem_id]) if params[:problem_id]
  end

  # GET /offers
  # GET /offers.json
  def index
    @offers = Offer
    @offers = @offers.where(:prof_id => @prof.id) if @prof
    @offers = @offers.where(:problem_id => @problem.id) if @problem
    @offers = @offers.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @offers }
    end
  end

  # GET /offers/1
  # GET /offers/1.json
  def show
    @offer = Offer.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @offer }
    end
  end

  # GET /offers/new
  # GET /offers/new.json
  def new
    @offer = Offer.new(:problem_id => @problem.id, :prof_id => @prof.id)

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @offer }
    end
  end

  # GET /offers/1/edit
  def edit
    @offer = Offer.find(params[:id])
  end

  # POST /offers
  # POST /offers.json
  def create
    @offer = Offer.new(params[:offer])

    respond_to do |format|
      if @offer.save
        #UserNotifier.send_users(@prof, @problem, @offer, "masterweily@gmail.com",request).deliver
        #UserNotifier.send_users(@prof, @problem, @offer, "nitzanav@gmail.com",request).deliver
        #UserNotifier.send_users(@prof, @problem, @offer, "masterweily@gmail.com",request).deliver
        #UserNotifier.send_users(@prof, @problem, @offer, "leoshpitz@shopnfly.com").deliver
        #UserNotifier.send_users(@prof, @problem, @offer, "guy@shopnfly.com").deliver
        UserNotifier.send_users(@prof, @problem, @offer, "guy.gaash@gmail.com",request).deliver

        format.html { redirect_to prof_problems_path(@prof), notice: 'Offer was successfully created.' }
        format.json { render json: @offer, status: :created, location: @offer }
      else
        format.html { render action: "new" }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /offers/1
  # PUT /offers/1.json
  def update
    @offer = Offer.find(params[:id])

    respond_to do |format|
      if @offer.update_attributes(params[:offer])
        format.html { redirect_to @offer, notice: 'Offer was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /offers/1
  # DELETE /offers/1.json
  def destroy
    @offer = Offer.find(params[:id])
    @offer.destroy

    respond_to do |format|
      format.html { redirect_to offers_url }
      format.json { head :no_content }
    end
  end
end
