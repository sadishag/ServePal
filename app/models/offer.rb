class Offer < ActiveRecord::Base
  attr_accessible :arrival, :free_text, :price, :problem_id, :prof_id

  belongs_to :prof
  belongs_to :problem
end
