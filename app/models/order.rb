class Order < ActiveRecord::Base

  belongs_to :offer

  attr_accessible :offer_id

  delegate :problem,:problem_id,:price, to: :offer
  delegate :title,:file,:description,:address, to: :problem

  def paypal_business
    offer.prof.email
  end
end
