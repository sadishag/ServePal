class Video < ActiveRecord::Base
  attr_accessible :file

  mount_uploader :file, VideoFileUploader
end
