class Problem < ActiveRecord::Base
  attr_accessible :address, :description, :file, :name
  has_and_belongs_to_many :categories

  has_many :offers
  belongs_to :location


  mount_uploader :file, VideoFileUploader

  def title
    name
  end



  def prices
    offers.pluck(:price).compact
  end

  def price_range
    if prices.size == 0
      ""
    elsif prices.size == 1
      "#{prices.first}$"
    else
      "#{prices.min}$ - #{prices.max}$"
    end
  end

  def partial_user_name
    "Guy G."
  end

  def categories_html
    return "" unless categories
    categories.map(&:name).join ", "
  end

  def first_category
    categories[0] if categories.present?
  end

end
